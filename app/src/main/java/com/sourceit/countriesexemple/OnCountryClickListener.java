package com.sourceit.countriesexemple;

import com.sourceit.countriesexemple.network.model.Country;

public interface OnCountryClickListener {
    void onClick(Country country);
}

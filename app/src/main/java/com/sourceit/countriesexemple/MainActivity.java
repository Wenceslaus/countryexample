package com.sourceit.countriesexemple;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewbinding.ViewBinding;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.sourceit.countriesexemple.databinding.ActivityMainBinding;
import com.sourceit.countriesexemple.network.ApiService;
import com.sourceit.countriesexemple.network.model.Country;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    private Disposable disposable;
    private ActivityMainBinding binding;
    private CountryAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        adapter = new CountryAdapter(this, this::showInfo);
        binding.recyclerList.setAdapter(adapter);
        disposable = ApiService.getData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::showInfo, this::showError);

    }

    public void showInfo(Country country) {
        InfoActivity.startActivity(this, country.name);
    }

    public void showInfo(List<Country> countries) {
        adapter.update(countries);
    }

    public void showError(Throwable t) {
        Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (disposable != null) {
            disposable.dispose();
        }
    }
}

package com.sourceit.countriesexemple.network.model;

public class Country {
    public String name;
    public String flag;
    public String capital;
    public String region;
    public String subregion;
    public String nativeName;
}

package com.sourceit.countriesexemple;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou;
import com.sourceit.countriesexemple.databinding.ActivityInfoBinding;
import com.sourceit.countriesexemple.network.ApiService;
import com.sourceit.countriesexemple.network.model.Country;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;

public class InfoActivity extends AppCompatActivity {

    private static final String EXTRA_COUNTRY_NAME = "country_name";

    public static void startActivity(Context context, String countryName) {
        Intent intent = new Intent(context, InfoActivity.class);
        intent.putExtra(EXTRA_COUNTRY_NAME, countryName);
        context.startActivity(intent);
    }


    public CompositeDisposable disposables = new CompositeDisposable();
    private ActivityInfoBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityInfoBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        if (getIntent().hasExtra(EXTRA_COUNTRY_NAME)) {
            String name = getIntent().getStringExtra(EXTRA_COUNTRY_NAME);

            disposables.add(ApiService.getCountryByName(name)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .filter(countries -> !countries.isEmpty())
                    .subscribe(countries -> {
                        Country country = countries.get(0);
                        binding.setCountry(country);
                        GlideToVectorYou
                                .init()
                                .with(binding.getRoot().getContext())
                                .load(Uri.parse(country.flag), binding.image);
                    }, throwable -> {
                        Toast.makeText(InfoActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    }));
            //request

        } else {
            //toast
            finish();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        disposables.clear();
    }
}

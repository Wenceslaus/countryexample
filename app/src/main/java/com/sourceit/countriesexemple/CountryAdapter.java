package com.sourceit.countriesexemple;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou;
import com.sourceit.countriesexemple.databinding.ItemCountryBinding;
import com.sourceit.countriesexemple.network.model.Country;

import java.util.ArrayList;
import java.util.List;

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.CountryHolder> {

    private Context context;
    private List<Country> originalList = new ArrayList<>();
    private List<Country> filterList = new ArrayList<>();
    private OnCountryClickListener listener;

    public CountryAdapter(Context context, OnCountryClickListener listener) {
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public CountryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemCountryBinding binding = ItemCountryBinding.inflate(LayoutInflater.from(context), parent, false);
        return new CountryHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CountryHolder holder, int position) {
        Country country = filterList.get(position);
        holder.binding.name.setText(country.name);
        holder.binding.capital.setText(country.capital);
        holder.binding.region.setText(country.region);
        holder.binding.subregion.setText(country.subregion);
        holder.binding.getRoot().setOnClickListener(v -> listener.onClick(country));
        GlideToVectorYou
                .init()
                .with(holder.binding.image.getContext())
                .load(Uri.parse(country.flag), holder.binding.image);
    }

    @Override
    public int getItemCount() {
        return filterList.size();
    }

    public void filter(String search) {
        filterList.clear();
        if (search.isEmpty()) {
            filterList.addAll(originalList);
        } else {
            for (Country country : originalList) {
                if (country.name.toLowerCase().contains(search.toLowerCase()) ||
                        country.capital.toLowerCase().contains(search.toLowerCase())) {
                    filterList.add(country);
                }
            }
        }
        notifyDataSetChanged();
    }

    public void update(List<Country> countries) {
        originalList.clear();
        originalList.addAll(countries);

        filterList.clear();
        filterList.addAll(countries);
        notifyDataSetChanged();
    }

    static class CountryHolder extends RecyclerView.ViewHolder {

        public ItemCountryBinding binding;

        public CountryHolder(ItemCountryBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
